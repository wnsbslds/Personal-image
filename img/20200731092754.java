package com.leyou.search;

import com.leyou.search.po.Goods;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * 使用SpringDataElasticSearch提供的工具接口
 * 包含CURD的基本方法
 */
public interface GoodsRepository extends ElasticsearchRepository<Goods,Long> {
}
